<?php
$errors = '';
/
$myemail = 'contact@mobbly.com.mx';
if(empty($_POST['name']) ||
   empty($_POST['email']) ||
   empty($_POST['phone']) ||
   empty($_POST['message']))
{
	$errors .= "\n Error: Todos los campos son requeridos";
}

$name = $_POST['name'];
$email_address = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email_address)) {
	$errors .= "\n Error: El correo es invalido";
}

if( empty($errors) ) {
	$to = $myemail;
	$email_subject = "Formulario de contacto Mobbly: $name";
	$email_body = "Has recivido un nuevo mensaje. ".
	" Direcion:\n Nombre: $name \n Correo: $email_address \n Telefono: $phone \n Mensaje: $message";

	$headers = "From: $myemail\n";
	$headers .= "De: $email_address";

	mail($to,$email_subject,$email_body,$headers);

	header('Location: thank-you.html');
}
?>
<!Doctype html>
<html lang="en">
<head>

	<title>Thank you!</title>

	<link rel="icon" href="imges/favicon.png">



<style>
h1 {
	font-family : 'Oxygen', Helvetica, sans-serif;
	font-size: 26px;
    letter-spacing: 2px;
    line-height: 42px;
    margin-bottom: 20px;
    font-weight: bold;	
}

label,a,body {
	font-family : 'Oxygen', Helvetica, sans-serif;
	font-size : 14px;
}
.respond {
	padding: 100px 0;
	text-align: center;
}
</style>	


</head>	
<body>

<div class="respond">
	<h1>Gracias!</h1>
	<p>Gracias por tu mensaje nos pondremos en contacto mtan pronto como nos sea posible</p>
</div>

</body>
</html>






